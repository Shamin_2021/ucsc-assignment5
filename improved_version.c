#include <stdio.h>
#include <math.h>
struct Cases {
   float price;
   int attendees;
   float profit;
   float revenue;
   float cost;
};


float revenue(int attendees,float ticketPrice){
  return attendees*ticketPrice;
}

float cost(int fixedcost,float varcost,float attendees){
  return fixedcost + (attendees* varcost);
}

float  profit(int attendes,float ticketPrice,float fixedcost,float varcost){
  return revenue(attendes,ticketPrice) - cost(fixedcost,varcost,attendes);
}

void  printrel(int attendes,float ticketPrice,float fixedcost,float varcost,struct Cases *cases){
  cases->revenue = revenue(attendes, ticketPrice);
  cases->cost = cost(fixedcost,varcost, attendes);
  cases->profit = profit(attendes,ticketPrice,fixedcost,varcost); 
  printf("\nthe revenue generated is  : %f",cases->revenue);
  printf("\nthe cost incurred is      : %f",cases->cost);
  printf("\nthe profit earned is      : %f",cases->profit);
}

float TicketAttendenceM(float price1,float price2,int attendence1,int attendence2)
{
  return (price1 - price2)/(attendence1 - attendence2);
}

float TicketAttendenceC(float price1,float price2,int attendence1,int attendence2)
{
  float demandPricegrad = (price1 - price2)/(attendence1 - attendence2);
  float demanpriceinter = price1 - (demandPricegrad*attendence1);
  return demanpriceinter;
}

float maxProft(float price1,float price2,int attendence1,int attendence2,float priceperAttendee){
  return (TicketAttendenceC(price1,price2,attendence1,attendence2) + priceperAttendee)/2;
}

float graphprofit(float p,float m,float c,float fc,float vc){
  return ((p*p/m)-(p*c/m)- (fc) -(3*(p-c)/m));
}

void drawGraph(float mp,float m,float c,float fc,float vc){
  float pftp[5][2];
  float sp = mp - 10;

  for(int j=0;j<5;j++){
      if(sp<=mp+10){
        pftp[j][0]= sp;
        pftp[j][1]= graphprofit(sp,m,c,fc,vc);
        sp=sp+5;
      }
  }

  printf("%10s %10s\n","profit|","ticketprice  ");
  printf("%21s|\n","  ");
  for(int j=0;j<5;j++){
        int k= round(pftp[j][1]/100);
        printf("%10.1f|",pftp[j][1]);
        printf("%10.1f|",pftp[j][0]);
        for(int y=0;y<k;y++){
          printf("=");
        }
        printf("\n");
  }
  printf("%21s|---------------------------->profit","  ");


}

void checkDemand(struct Cases *case1, struct Cases *case2,float fixedprice, float variableprice){
  int flag;
  float fp,pricepa,m,c;

  if((case1->price >case2->price && case1->attendees < case2->attendees) || (case2->price>case1->price && case2->attendees < case1->attendees)){
    printf("\n\n---ANALYSIS OF DECREASE IN PRICE--- ");
    if(case1->price>case2->price){
    printf("\nwhen decrease price by  %.1f ",case1->price-case2->price);
    printf("\nATTENDANCE | inccrease by  %d ",case2->attendees-case1->attendees);
    printf("\nREVENUE    | ");
    if(case1->revenue > case2->revenue)
    {
      printf("decrease by %f",case1->revenue - case2->revenue);
    }
    else{
      printf("increase by %f",case2->revenue - case1->revenue);
    }
    printf("\nCOST       | ");
    if(case1->cost > case2->cost)
    {
      printf("decrease by %f",case1->cost - case2->cost);
    }
    else{
      printf("increase by %f",case2->cost - case1->cost);
    }
    printf("\nPROFIT     | ");
    if(case1->profit > case2->profit)
    {
      printf("decrease by %f",case1->profit - case2->profit);
      flag=0;
    }
    else{
      printf("increase by %f",case2->profit - case1->profit);
      flag=1;
    }

    }
    if(case2->price>case1->price){
    printf("\nwhen decrease price by  %.1f ",case2->price-case1->price);
    printf("\nATTENDENCE | inccrease by  %d ",case1->attendees-case2->attendees);
    printf("\nREVENUE    | ");
    if(case2->revenue > case1->revenue)
    {
      printf("decrease by %f",case2->revenue - case1->revenue);
    }
    else{
      printf("increase by %f",case1->revenue - case2->revenue);
    }
    printf("\nCOST       | ");
    if(case2->cost > case1->cost)
    {
      printf("decrease by %f",case2->cost - case1->cost);
    }
    else{
      printf("increase by %f",case1->cost - case2->cost);
    }
    printf("\nPROFIT     | ");
    if(case2->profit > case1->profit)
    {
      printf("decrease by %f",case2->profit - case1->profit);
      flag=0;
    }
    else{
      printf("increase by %f",case1->profit - case2->profit);
      flag=1;
    }
    }

    if (flag ==1){
      printf("\n||with a decrease in ticket price the profit has increased ||");
    }
    if (flag ==0){
      printf("\n||with a decrease in ticket price the profit has decreased ||");
    }

    float mP= maxProft(case1->price,case2->price,case1->attendees,case2->attendees,variableprice);
    m = TicketAttendenceM(case1->price,case2->price,case1->attendees,case2->attendees);
    c = TicketAttendenceC(case1->price,case2->price,case1->attendees,case2->attendees);
    printf("\n\n use the below graph to visualize your profit with ticket price \n ");
    drawGraph(mP,m,c,fixedprice,variableprice);
    printf("\n\nThe ticket price to maximize profit : %f",mP);
  }else{
    printf("\n\n---the no.of attendees decreased with  a decrease in price--- \n CANT ANALYZE PRICE AND PROFIT RELATIONSHIP");
  }
}




int main(void) {
  int a;
  float fp,pricepa,m,c;

  struct Cases Case1;
  struct Cases Case2;
  

  printf("enter 1st price                : ");
  scanf("%f",&Case1.price);
  printf("enter attendence for 1st price : ");
  scanf("%d",&Case1.attendees);
  printf("enter 2st price                : ");
  scanf("%f",&Case2.price);
  printf("enter attendence for 2st price : ");
  scanf("%d",&Case2.attendees);
  printf("enter cost per attendee        : ");
  scanf("%f",&pricepa);
  printf("enter fixed cost of performance: ");
  scanf("%f",&fp);

  

  printf("\n\n------scenario 1----------  || priceofticket: %.1f",Case1.price);
  printrel(Case1.attendees,Case1.price,fp,pricepa,&Case1);

  printf("\n\n------scenario 2----------  || priceofticket: %.1f",Case2.price);
  printrel(Case2.attendees,Case2.price,fp,pricepa,&Case2);

  checkDemand(&Case1,&Case2,fp,pricepa);

  


  
  

}
