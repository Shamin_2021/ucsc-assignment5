#include <stdio.h>


float revenue(float attendees,float ticketPrice){
  return attendees*ticketPrice;
}

float cost(float fixedcost,float varcost,float attendees){
  return fixedcost + (attendees* varcost);
}

float  profit(float attendes,float ticketPrice,float fixedcost,float varcost){
  return revenue(attendes,ticketPrice) - cost(fixedcost,varcost,attendes);
}

void  printrel(float attendes,float ticketPrice,float fixedcost,float varcost){
  printf("\nthe revenue generated is  : %f",revenue(attendes, ticketPrice));
  printf("\nthe cost incurred is      : %f",cost(fixedcost,varcost, attendes));
  printf("\nthe profit earned is      : %f",profit(attendes,ticketPrice,fixedcost,varcost));
}

float TicketAttendenceM(float price1,float price2,float attendence1,float attendence2)
{
  return (price1 - price2)/(attendence1 - attendence2);
}

float TicketAttendenceC(float price1,float price2,float attendence1,float attendence2)
{
  float demandPricegrad = (price1 - price2)/(attendence1 - attendence2);
  float demanpriceinter = price1 - (demandPricegrad*attendence1);
  return demanpriceinter;
}

float maxProft(float price1,float price2,float attendence1,float attendence2,float priceperAttendee){
  return (TicketAttendenceC(price1,price2,attendence1,attendence2) + priceperAttendee)/2;
}

float graphprofit(float p,float m,float c,float fc,float vc){
  return ((p*p/m)-(p*c/m)- (fc) -(3*(p-c)/m));
}

void drawGraph(float mp,float m,float c,float fc,float vc){
  float pftp[5][2];
  float sp = mp - 10;

  for(int j=0;j<5;j++){
      if(sp<=mp+10){
        pftp[j][0]= sp;
        pftp[j][1]= graphprofit(sp,m,c,fc,vc);
        sp=sp+5;
      }
  }

  printf("%15s\n","ticketprice  ");
  printf("%10s|\n","  ");
  for(int j=0;j<5;j++){
        int k= pftp[j][1]/100;
        printf("%10.1f|",pftp[j][0]);
        for(int y=0;y<k;y++){
          printf("=");
        }
        printf("%f\n",pftp[j][1]);
  }
  printf("%10s|---------------------------->profit","  ");


}

int main(void) {
  int a;
  float fp,pricepa,m,c;
  float attendencePrice[2][2];

  printf("enter 1st price                : ");
  scanf("%f",&attendencePrice[0][0]);
  printf("enter attendence for 1st price : ");
  scanf("%f",&attendencePrice[0][1]);
  printf("enter 2st price                : ");
  scanf("%f",&attendencePrice[1][0]);
  printf("enter attendence for 2st price : ");
  scanf("%f",&attendencePrice[1][1]);
  printf("enter cost per attendee        : ");
  scanf("%f",&pricepa);
  printf("enter fixed cost of performance: ");
  scanf("%f",&fp);

  printf("\n\n------scenario 1----------  || priceofticket: %.1f",attendencePrice[0][0]);
  printrel(attendencePrice[0][1],attendencePrice[0][0],fp,pricepa);

  printf("\n\n------scenario 2----------  || priceofticket: %.1f",attendencePrice[1][0]);
  printrel(attendencePrice[1][1],attendencePrice[1][0],fp,pricepa);



  float mP= maxProft(attendencePrice[0][0],attendencePrice[1][0],attendencePrice[0][1],attendencePrice[1][1],pricepa);




  m = TicketAttendenceM(attendencePrice[0][0],attendencePrice[1][0],attendencePrice[0][1],attendencePrice[1][1]);
  c = TicketAttendenceC(attendencePrice[0][0],attendencePrice[1][0],attendencePrice[0][1],attendencePrice[1][1]);
  printf("\n\n use the below graph to visualize your profit with ticket price \n ");
  drawGraph(mP,m,c,fp,pricepa);

  printf("\n\nThe ticket price to maximize profit : %f",mP);








}
